package main

import (
    "fmt"
    "net/http"
    "encoding/json"
    "log"
    "io/ioutil"
    "github.com/julienschmidt/httprouter"
)

type Message struct {
  UserKey string `json:"user_key"`
  Type string `json:"type"`
  Content string `json:"content"`
}

func Response(w http.ResponseWriter, body string) {
    w.Header().Set("Content-Type", "application/json; charset=utf-8")
    fmt.Fprintf(w, "%s", body)
}

func ResponseMessage(w http.ResponseWriter, msg string) {
    w.Header().Set("Content-Type", "application/json; charset=utf-8")
    fmt.Fprintf(w, "{\"message\":{\"text\": \"%s\"}}", msg)
}

func keyboardHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    Response(w, "{\"type\": \"text\"}")
}

func messageHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    var msg Message
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
      log.Println("ERR: Failed to read message")
      defer r.Body.Close()
      ResponseMessage(w, "저장 실패")
      return
    }
    if err := json.Unmarshal(body, &msg); err != nil {
      log.Println("ERR: failed to unmarshal message")
      ResponseMessage(w, "저장 실패")
    }
    log.Println("INFO: message: ", msg.Content)
    ResponseMessage(w, "저장 성공")
}

func friendHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    if r.Method == "POST" {
      log.Println("INFO: friend added")
    } else {
      log.Println("INFO: friend deleted")
    }
    rs := "{\"message\": \"ok\"}"
    Response(w, rs)
}

func chatroomHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    log.Println("INFO: chatroom deleted")
    rs := "{\"message\": \"ok\"}"
    Response(w, rs)
}

func main() {
    router := httprouter.New()
    router.GET("/keyboard", keyboardHandler)
    router.POST("/message", messageHandler)
    router.POST("/friend", friendHandler)
    router.DELETE("/friend/:user_key", friendHandler)
    router.DELETE("/chat_room/:user_key", chatroomHandler)
    http.ListenAndServe(":5010", router)
}
