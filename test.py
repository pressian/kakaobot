import json

import requests

user_msg = {
    "user_key": "encryptedUserKey",
    "type": "text",
    "content": "차량번호등록",
}

user_info = {
    "user_key": "encryptedUserKey",
}

r = requests.get("http://127.0.0.1:5010/keyboard")
print(r.text)

r = requests.post("http://127.0.0.1:5010/message", data=json.dumps(user_msg))
print(r.text)

r = requests.post("http://127.0.0.1:5010/friend", data=json.dumps(user_info))
print(r.text)

r = requests.delete("http://127.0.0.1:5010/friend/encryptedUserKey")
print(r.text)

r = requests.delete("http://127.0.0.1:5010/chat_room/encryptedUserKey")
print(r.text)
